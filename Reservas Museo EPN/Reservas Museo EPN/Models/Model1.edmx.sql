
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 01/28/2018 15:04:37
-- Generated from EDMX file: C:\Users\Danni\Documents\Source\Repos\Reservas Museo\Reservas Museo EPN\Reservas Museo EPN\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Reservas];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ReservaSet'
CREATE TABLE [dbo].[ReservaSet] (
    [IdReserva] int IDENTITY(1,1) NOT NULL,
    [NombreCliente] nvarchar(max)  NOT NULL,
    [TelefonoCliente] nvarchar(max)  NOT NULL,
    [CorreoCliente] nvarchar(max)  NOT NULL,
    [NumeroPersonas] nvarchar(max)  NOT NULL,
    [InstitucionCliente] nvarchar(max)  NOT NULL,
    [HoraReserva] nvarchar(max)  NOT NULL,
    [EstadoReserva] nvarchar(max)  NOT NULL,
    [Fecha_IdDia] int  NOT NULL,
    [Usuario_IdUsuario] int  NOT NULL
);
GO

-- Creating table 'UsuarioSet'
CREATE TABLE [dbo].[UsuarioSet] (
    [IdUsuario] int IDENTITY(1,1) NOT NULL,
    [NombreUsuario] nvarchar(max)  NOT NULL,
    [ContraseñaUsuario] nvarchar(max)  NOT NULL,
    [CargoUsuario] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'FechaSet'
CREATE TABLE [dbo].[FechaSet] (
    [IdDia] int IDENTITY(1,1) NOT NULL,
    [Anio] nvarchar(max)  NOT NULL,
    [Mes] nvarchar(max)  NOT NULL,
    [Dia] nvarchar(max)  NOT NULL,
    [EstadoFecha] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [IdReserva] in table 'ReservaSet'
ALTER TABLE [dbo].[ReservaSet]
ADD CONSTRAINT [PK_ReservaSet]
    PRIMARY KEY CLUSTERED ([IdReserva] ASC);
GO

-- Creating primary key on [IdUsuario] in table 'UsuarioSet'
ALTER TABLE [dbo].[UsuarioSet]
ADD CONSTRAINT [PK_UsuarioSet]
    PRIMARY KEY CLUSTERED ([IdUsuario] ASC);
GO

-- Creating primary key on [IdDia] in table 'FechaSet'
ALTER TABLE [dbo].[FechaSet]
ADD CONSTRAINT [PK_FechaSet]
    PRIMARY KEY CLUSTERED ([IdDia] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Fecha_IdDia] in table 'ReservaSet'
ALTER TABLE [dbo].[ReservaSet]
ADD CONSTRAINT [FK_FechaReserva]
    FOREIGN KEY ([Fecha_IdDia])
    REFERENCES [dbo].[FechaSet]
        ([IdDia])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FechaReserva'
CREATE INDEX [IX_FK_FechaReserva]
ON [dbo].[ReservaSet]
    ([Fecha_IdDia]);
GO

-- Creating foreign key on [Usuario_IdUsuario] in table 'ReservaSet'
ALTER TABLE [dbo].[ReservaSet]
ADD CONSTRAINT [FK_UsuarioReserva]
    FOREIGN KEY ([Usuario_IdUsuario])
    REFERENCES [dbo].[UsuarioSet]
        ([IdUsuario])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsuarioReserva'
CREATE INDEX [IX_FK_UsuarioReserva]
ON [dbo].[ReservaSet]
    ([Usuario_IdUsuario]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------