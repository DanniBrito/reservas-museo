﻿using System.Web;
using System.Web.Mvc;

namespace Reservas_Museo_EPN
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
